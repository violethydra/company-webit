Описание проекта
===

## СБОРКА ПРОЕКТА
	"name": "ROOT",
	"version": "1.0.5",

#### СТРУКТУРА
	- development → сборочная область для формирования цельного кода
	- public → сгенерированная итоговая область
	- __resource → хранение ТЗ и PSD макетов
	- Readme.md → git-редми файл
	- package.json → список зависимостей в проекте
	- PROJECT.sublime-workspace → рабочая область для SublimeText
	- PROJECT.sublime-project → рабочая область для SublimeText
	- gulpfile.babel.js → сборочные таски
	- gulpfile.config → (...в разработке)
	- gulpfile.config.json → (...в разработке)
	- .gitattributes → корректировка для git
	- .gitconfig → бекап алиасов
	- .gitignore → игнорфайл для git
	- Contributing.md → описание
	- Changelog → список изменений
	- License → лицензия
	- .gitlab-ci.yml → тесты для gitlab
	- .eslintrc → конфиг для линтинга
	- .tidyconfig → конфиг для линтинга html
	- .babelrc → мост для babel

#### HTML
	- include → layouts / встраивание частей страниц в index.html

#### CSS
	- stylus → препроцессор с расширением .styl | .stylus

#### JS
	- webpack → javascript собирается в связке с babel
module.exports = class AddLogicCarousel {
	constructor(enter, init, gap, drag) {
		this.enter = enter;
		this.drag = drag;
		this.gap = gap;
		this.init = {
			0: { items: init.sm },
			768: { items: init.md },
			992: { items: init.lg },
			1200: { items: init.xl },
			1600: { items: init.xxl }
		};
	}

	static info() {
		console.log('MODULE:', this.name, true);
	}

	run() {
		const elem = document.querySelector(`${this.enter}`);
		// const output = document.querySelector(`${this.exit}`);
		if (elem) {
			this.constructor.info();
			const init = {
				loop: true,
				margin: this.gap,
				nav: false,
				dots: false,
				rewind: false,
				touchDrag: this.drag,
				mouseDrag: this.drag,
				merge: false,
				responsive: this.init
			};

			$(this.enter).owlCarousel(init);
			elem.style.visibility = 'visible';

			const control = (event, name) => {
				event.preventDefault();
				$(this.enter).trigger(`${name}.owl.carousel`);
			};

			$(`${this.enter}Navigation`).eq(1).on('click', event => control(event, 'next'));
			$(`${this.enter}Navigation`).eq(0).on('click', event => control(event, 'prev'));
		}
	}
};
